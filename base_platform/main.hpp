
#ifndef _main_h
#define _main_h

#include  "mips/mips_isa.H"

/*para a função em offload*/
#define BITSPERLONG 32
#define TOP2BITS(x) ((x & (3L << (BITSPERLONG-2))) >> (BITSPERLONG-2))


/*to help debug the program*/
//#define DEBUG
//#define DEBUGll

/*return this cpu identifier*/
int getCPUID(mips_parms::mips_isa * isa);

/*start another cpu on this instruction*/
void startCPUID(mips_parms::mips_isa * isa, int id/*, unsigned addr*/);

/*start all cpu on this instruction*/
void startallCPUID(mips_parms::mips_isa * isa/*, unsigned addr*/);

/*pause a cpu*/
void pauseCPUID(int id);

/*unpause a cpu*/
void unpauseCPUID(int id);

/*wait for another cpu to finish*/
void joinCPUID(int id);

/*wait for all cpus to finish*/
void joinallCPUID(mips_parms::mips_isa * isa);

/*check if a cpu is paused*/
bool isrunningCPUID(int id);

/*the core is waiting to exit*/
bool iswaitingexitCPUID(int id);

/*stops a cpu*/
void stopCPU(mips_parms::mips_isa * isa);

/*check if a core may run, if not wait for a signal*/
void waitFor(mips_parms::mips_isa * isa);

/*the offloaded code*/
void offLoaded(mips_parms::mips_isa * isa, unsigned long x);

/*get the sqrt and frac values from the offloaded code*/
unsigned getVal(mips_parms::mips_isa * isa, int par);

#endif
