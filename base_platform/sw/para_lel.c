

#include "para_lel.h"

#include <stdio.h>
#include <stdlib.h>

int (* run_at[8])();

void unlockCPU(int id)
{
	int * addr = (int *) (0xFFFFFF00 + id);
	int i = *(addr);
    //asm ("lw $a0, 0xFFFFFF00($a0)");
}

/*indicate the running address and unpause a cpu*/
void startCPUat(int id, void * pos)
{
	#ifdef DEBUG
	printf("%d will start at %p\n", id, pos);
	#endif
	
    run_at[id] = pos;
    unlockCPU(id);
}

/*indicates that a cpu is stopped*/
int stopCPU()
{
	int * addr = (int *) 0xFFFFFFFF;
	int i = *addr;
	//asm ("lw $v0, 0xFFFFFFFF($0)");
    exit(0);
    return 0;
}

/*return the cpu id*/
int getCPUID()
{
	int * addr = (int *) 0xFFFFFF0A;
	return *addr;
	//asm ("lw $v0, 0xFFFFFF0A($0)\nj $ra");	
}

void waitFor()
{
	int * addr = (int *) 0xFFFFFFDD;
	int i = *addr;
    //asm("lw $t7, 0xFFFFFFDD($0)");
}

/*upon program start...*/
void startProgram()
{
    /*go until this point and wait*/
    waitFor();
    
    /*if the function is not the main one, run at specific address*/
    int p = getCPUID();
    if(p != 0)
    {
        run_at[p]();
        stopCPU();
    }
}

/*will wait for the other cpus to finish*/
void joinallCPUID()
{
	int * addr = (int *) 0xFFFFFFA8;
	int i = *addr;
	//asm ("lw $t7, 0xFFFFFFA8($0)");
}
