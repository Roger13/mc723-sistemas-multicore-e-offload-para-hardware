#include <stdio.h>
#include <stdlib.h>
#include "para_lel.h"

#define offload
//#define umCore
#define doisCores
//#define quatroCores
//#define oitoCores

/*para a função em offload*/
#define BITSPERLONG 32
#define TOP2BITS(x) ((x & (3L << (BITSPERLONG-2))) >> (BITSPERLONG-2))

unsigned resultsSqrt [800];
unsigned resultsFrac [800];

void progC(int inicio, int fim);
void lock();

int ll(int * pos);
int sc(int val, int * pos);

int ll(int * pos)
{
	asm("ll $v0, 0($a0)");
	return;
}

int sc(int val, int * pos)
{
	int res;
	asm ( "sc %1, 0(%2);"
				"addi %0, %1, 0;"
		: "=r" ( res )                    // output
		: "r" ( val ), "r" ( pos )        // input
	);
	return res;
}

int semaphor = 0;

void lock(){

    int * addr_lock = & semaphor;

    int ret = 0;
    while(ret == 0)
    {
        while(*addr_lock == 1); //enquanto estiver travado

        ll(addr_lock);
        if(*addr_lock == 1) //se estiver travado
            continue;
        
        ret = sc(1, addr_lock);
        #ifdef DEBUGll
		if(ret == 1)
			printf("sc: %d | addr_lock = %d\n", ret, addr_lock);
		#endif
    }
}
	
void release(){
    semaphor = 0;
}
	
int f0()
{   
    #ifdef umcore
        progC(1,800);
    #endif
    #ifdef doisCores
        progC(1,400);
    #endif
    #ifdef quatroCores
        progC(1,200);
    #endif
    #ifdef oitoCores
        progC(1,100);
    #endif
    return 0;
}

int f1()
{
    #ifdef doisCores
        progC(401,800);
    #endif
    #ifdef quatroCores
        progC(201,400);
    #endif
    #ifdef oitoCores
        progC(101,200);
    #endif
	//printf("hi from 1!\n");
    return 0;
}

int f2()
{
    #ifdef quatroCores
        progC(401,600);
    #endif
    #ifdef oitoCores
        progC(201,300);
    #endif
	//printf("hi from 2!\n");
    return 0;
}

int f3()
{
    #ifdef quatroCores
        progC(601,800);
    #endif
    #ifdef oitoCores
        progC(301,400);
    #endif
    //printf("3: :O!\n");
    return 0;
}   

int f4()
{
    #ifdef oitoCores
        progC(401,500);
    #endif //<---- isto não funciona (substituindo a função e o bloco)
    //printf("4: hey!\n");
    return 0;
}

int f5()
{    
    #ifdef oitoCores
        progC(501,600);
    #endif
    //printf("5: herp derp!\n");
    return 0;
}

int f6()
{
    #ifdef oitoCores
        progC(601,700);
    #endif
    //printf("6: me is 6!\n");
    return 0;
}

int f7()
{
    #ifdef oitoCores
        progC(701,800);
    #endif
    //printf("7: gotta go!\n");
    return 0;
}

void progC(int inicio, int fim)
{
    #ifdef offload
    int i;
    for (i = inicio; i <= fim; ++i)
    {
        //usqrt(i, &q);
        //Executa usqrt em "hardware"
        lock();
        #ifdef DEBUGll
        printf("%d: peguei o lock\n", getCPUID());
        #endif
        
        int * addrI = (int *) 0xFFFFFFC0; 
        *addrI = i;

        int * addr = (int *) 0xFFFFFFCC;
        resultsSqrt[i] = *addr;
        int * addr2 = (int *) 0xFFFFFFCD;
        resultsFrac[i] = *addr2;
        
        #ifdef DEBUGll
        printf("%d: vou soltar o lock\n", getCPUID());
        #endif
        release();
    }
    #else
    int t, x;
    for (t = inicio; t <= fim; t++){
        x = t;
        
        unsigned long a = 0L;                   /* accumulator      */
        unsigned long r = 0L;                   /* remainder        */
        unsigned long e = 0L;                   /* trial product    */
        
        int i;

        for (i = 0; i < BITSPERLONG; i++)   /* NOTE 1 */
        {
            r = (r << 2) + TOP2BITS(x); x <<= 2; /* NOTE 2 */
            a <<= 1;
            e = (a << 1) + 1;
            if (r >= e)
            {
                r -= e;
                a++;
            }
        }
        resultsFrac[t] = (unsigned) (a);
        resultsSqrt[t] = (unsigned) (a >> 16);
    }

    #endif
    return;
}


int main(int argc, char *argv[])
{
	/** Inicialização **/
	int p;
	
	startProgram();
	p = getCPUID();

	printf("%d: Iniciando programa\n", p);
    
	startCPUat(1, &f1);
	startCPUat(2, &f2);
	startCPUat(3, &f3);
	startCPUat(4, &f4);
	startCPUat(5, &f5);
	startCPUat(6, &f6);
	startCPUat(7, &f7);
    
    f0();

	joinallCPUID();
    
    int i = 1;
    for(i = 1; i < 800; i++){
        printf("Sqrt de %d: %d - Frac de %d: %d\n",i,resultsSqrt[i], i, resultsFrac[i]);
    }
    
	printf("0: jobs done!\n");
	
	stopCPU();
	
	return 0;
}
