/********************************************************************************
*						      			       				   
* 																Basicmath				                                
*											Parallel 1.0 Version with Pthread
*															
* Authors:           	Yuchen Liang and Syed Muhammad Zeeshan Iqbal
* Supervisor:					Hakan Grahn	 						
* Section:          	Computer Science of BTH,Sweden
* GCC Version:				4.2.4								
* Environment: 				Kraken(Ubuntu4.2.4) with Most 8 Processors 				
* Start Date:       	15th September 2009					
* End Date:         	6th October 2009			
*		
*********************************************************************************/

#include "snipmath.h"
#include <math.h>
#include <stdio.h>
#include <pthread.h>
#define  PRINT  1 // Print Switch

#include "params.h"


#define 		ISQRT			2


// Switch to do large or small calculations.
#define     LARGE_DATA_SET  1 
#define     SMALL_DATA_SET  2 


#define     ONE_KILO_NUMBERS 1024
#define     ONE_MEGA_NUMBERS 1024*1024
#define     ONE_GIGA_NUMBERS 1024*1024*1024

pthread_attr_t string_attr;
pthread_t workers[PROCESSORS];
int math_operation;
int data_set_type;
int no_workers;

typedef struct Ilimits
{ // Limits of ineger type
		int _start;
		int _end;
		int _offset;
}ilimit;

typedef struct ULlimits
{ // Limits of unsigned long type
	  unsigned long _start;
	  unsigned long _end;
	  unsigned long _offset;
}ullimit;

// Limits of double type
typedef struct DoubleLimits
{
	   double _start;
	   double _end;
	   double _offset;
}dlimit;

static ilimit  _ilim,_ilimParams[PROCESSORS];
static ullimit _ullim,_ullimParams[PROCESSORS];
static dlimit  _dlim1,_dlim2,_dlimParams1[PROCESSORS],_dlimParams2[PROCESSORS];
static dlimit  _adlim,_bdlim,_cdlim,_ddlim,_adlimParams[PROCESSORS];  // double type limits for variable's a,b,c,d

void intializeLimits()
{
	if (data_set_type==LARGE_DATA_SET){
		//Initialization of integer type limits
		_ilim._start=0;
		_ilim._end=  1*ONE_GIGA_NUMBERS;
		_ilim._offset=1;

		//Initialization of unsigned long type limits
		_ullim._start=1072497001;
		_ullim._end=1072497001+ (1*ONE_GIGA_NUMBERS);
		_ullim._offset=1;

		//Initialization of double type limits
		_adlim._start=1.0,_adlim._end=16.0,_adlim._offset=1e-10;
		_bdlim._start=10.0,_bdlim._end=10.0,_bdlim._offset=0.25;
		_cdlim._start=5.0,_cdlim._end=15.0,_cdlim._offset=0.61;
		_ddlim._start=-1.0,_ddlim._end=-5.0,_ddlim._offset=0.451;

		_dlim1._start=0.0;
		_dlim1._end=360.0;
		_dlim1._offset=1e-9;

		_dlim2._start=0.0;
		_dlim2._end=(6 * PI + 1e-6);
		_dlim2._offset=(PI / 1e11);
	}
	else if (data_set_type==SMALL_DATA_SET)
	{
		//Initialization of integer type limits
		_ilim._start=0;
		_ilim._end=1*ONE_MEGA_NUMBERS;
		_ilim._offset=1;

		//Initialization of unsigned long type limits
		_ullim._start=1072497001;
		_ullim._end=  1072497001 +(1*ONE_MEGA_NUMBERS);
		_ullim._offset=1;

		//Initialization of double type limits
		_adlim._start=1.0,_adlim._end=10.0,_adlim._offset=1e-9;
		_bdlim._start=10.0,_bdlim._end=10.0,_bdlim._offset=0.25;
		_cdlim._start=5.0,_cdlim._end=15.0,_cdlim._offset=0.61;
		_ddlim._start=-1.0,_ddlim._end=-5.0,_ddlim._offset=0.451;

		_dlim1._start=0.0;
		_dlim1._end=360.0;
		_dlim1._offset=1e-8;

		_dlim2._start=0.0;
		_dlim2._end=(6 * PI + 1e-6);
		_dlim2._offset=(PI /1e10);
	}
}


void calcISqrt(int workerID){
	int index;
	struct int_sqrt q;

	printf("********* CALCULATE INTEGER SQR ROOTS : Worker %d  ***********\n",workerID+1);

	for (index = _ilimParams[workerID]._start; index < _ilimParams[workerID]._end; index+=_ilim._offset)
	{
		usqrt(index, &q);
		// remainder differs on some machines
		if (PRINT)
			printf("sqrt(%3d) = %2d , at Worker %d\n",index, q.sqrt, workerID+1);
	}
}

void init_workers()
{
	pthread_attr_init(&string_attr);
	pthread_attr_setdetachstate(&string_attr,PTHREAD_CREATE_JOINABLE);
}

void *doWork(void *workerID){
	long id = (long)workerID;

	if (PRINT)
		printf("Worker %d started work.\n",id+1);  

	//Each thread performs following five tasks according to assigned work.
	if (math_operation ==ISQRT)
		calcISqrt(id);
	else
		printf("Undefined math operation\n");	

	if (PRINT)
		printf("Worker %d has completed work.\n",id);
}

void createWorkers(){
	long index;
	for ( index= 0 ; index < no_workers; index++ )
		pthread_create(&workers[index],&string_attr,doWork,(void *)index);
}
void waitForWorkersToFinish()
{
	int index;
	for ( index= 0 ; index < no_workers ; index++ )
		pthread_join(workers[index],NULL);
}                                               
 
void divide_into_sub_tasks()
{
	int i,temp;
	int isindex;
	unsigned long ulsindex,ulTemp;
	double dsindex1,dsindex2,adsindex;

	adsindex = _adlim._start;
	isindex = _ilim._start;
	ulsindex = _ullim._start;

	dsindex1= _dlim1._start;
	dsindex2= _dlim2._start;

	// Total long numbers
	ulTemp=_ullim._end-_ullim._start;

	for (i=0;i<no_workers;i++)
	{
		_adlimParams[i]._start=adsindex;
		_adlimParams[i]._end = _adlimParams[i]._start+(_adlim._end/no_workers);
		adsindex+=(_adlim._end/no_workers);

		_ilimParams[i]._start=isindex;
		_ilimParams[i]._end = _ilimParams[i]._start+(_ilim._end/no_workers);
		isindex+=(_ilim._end/no_workers);

		_ullimParams[i]._start=ulsindex;
		_ullimParams[i]._end = _ullimParams[i]._start+(ulTemp/no_workers);
		ulsindex+=(ulTemp/no_workers);

		_dlimParams1[i]._start=dsindex1;
		_dlimParams1[i]._end = _dlimParams1[i]._start+(_dlim1._end/no_workers);
		dsindex1+=(_dlim1._end/no_workers);

		temp=_dlim2._end;
		_dlimParams2[i]._start=dsindex2;
		_dlimParams2[i]._end = _dlimParams2[i]._start+(temp/no_workers);
		dsindex2+=(temp/no_workers);

	}
	// It handles even and odd number total work.
	_ilimParams[i-1]._end =_ilimParams[i-1] . _end+(_ilim._end%no_workers);
	_ullimParams[i-1]._end=_ullimParams[i-1]._end+(_ullim._end%no_workers);
}

void show_sub_tasks()
{
	int i;
	printf("Tasks for Integer Type Square Roots!\n");
	for (i=0;i<no_workers;i++)
	{
		printf("%d   ",_ilimParams[i]._start);
		printf("%d   \n",_ilimParams[i]._end);
	}
}


/* The printf's may be removed to isolate just the math calculations */

int main(void)
{
	no_workers = 8;
	data_set_type = SMALL_DATA_SET; //SMALL_DATA_SET | LARGE_DATA_SET
	math_operation = ISQRT;
	
	intializeLimits();
	divide_into_sub_tasks();
	
	init_workers();
	createWorkers();
	waitForWorkersToFinish();
	
	puts("Finish Working");

	return 0;
}
