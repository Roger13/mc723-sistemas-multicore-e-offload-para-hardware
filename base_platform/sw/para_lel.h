
#ifndef para_lel_h_
#define para_lel_h_

extern int (* run_at[8])();

/*to help debug the program*/
//#define DEBUG

/*indicate the running address and unpause a cpu*/
void startCPUat(int id, void * pos);

/*indicates that a cpu is stopped*/
int stopCPU();

/*return the cpu id*/
int getCPUID();

/*upon program start...*/
void startProgram();

/*will wait for the other cpus to finish*/
void joinallCPUID();

#endif
